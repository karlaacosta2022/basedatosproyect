/*CONSULTAS*/

/* Mostrar a todos los empleados desde el de más antigüedad hasta el de menos antigüedad. 
Debe aparecer en una columna nombres y apellidos, 
en otra columna el tiempo que ha trabajado en la cadena en años, meses y días.
En otra columna debe aparecer el mensaje “activo” o “inactivo”,
para saber si está o no trabajando en la empresa */
SELECT
vendedor.ven_nombres,vendedor.ven_apellidos,
age( NOW(),vendedor.ven_fechaingreso) as tiempo,estadovendedor.estado_descripcion
FROM VENDEDOR INNER JOIN SUCURSAL ON VENDEDOR.SUC_ID=SUCURSAL.SUC_ID
INNER JOIN ESTADOVENDEDOR ON ESTADOVENDEDOR.ESTADO_ID = VENDEDOR.ESTADO_ID
order by vendedor.ven_fechaingreso

/*
Mostrar los datos de pago de los empleados.
Debe aparecer en una columna nombres y apellidos del empleado, 
en otra columna la cantidad de horas trabajadas por el empleado, en otra columna 
la cantidad de dinero que se le ha pagado al empleado,
el horario que tiene y la sucursal donde trabaja
*/

SELECT
vendedor.ven_nombres,vendedor.ven_apellidos,
(EXTRACT(Epoch FROM current_timestamp) - EXTRACT(Epoch FROM ven_fechaingreso))/3600 as horas,
(EXTRACT(Epoch FROM current_timestamp) -
 EXTRACT(Epoch FROM ven_fechaingreso))/3600 * horariolaboral.pago_hora as pago,horariolaboral.horario_entrada,
horariolaboral.horario_salida,sucursal.suc_nombre
FROM VENDEDOR INNER JOIN SUCURSAL ON VENDEDOR.SUC_ID=SUCURSAL.SUC_ID
INNER JOIN ESTADOVENDEDOR ON ESTADOVENDEDOR.ESTADO_ID = VENDEDOR.ESTADO_ID
INNER JOIN HORARIOLABORAL ON HORARIOLABORAL.HORARIO_ID = VENDEDOR.HORARIO_ID

/*Mostrar la adquisición de fármacos de cada sucursal
la farmacéutica de donde obtienen los fármacos junto al proveedor quien los provee,
la fecha donde adquirieron el fármaco e información relevante del fármaco adquirido. 
*/
SELECT 
sucursal.suc_nombre,
farmaceutica.farma_nombre,
proveedor.prov_nombre,
adquisicionfarmaco.adqui_fecha,
adquisicionfarmaco.adqui_costo,
farmaco.farmaco_nombre,
farmaco.farmaco_precio
FROM sucursal inner join farmaco on sucursal.suc_id= farmaco.suc_id
inner join adquisicionfarmaco on adquisicionfarmaco.farmaco_id =  farmaco.farmaco_id
inner join proveedor on proveedor.prov_id = adquisicionfarmaco.prov_id
inner join farmaceutica on farmaceutica.farma_id = proveedor.farma_id

/*Mostrar las detalles de ventas de cada sucursal,
la información de los clientes que compraron,
fármaco y el cantidad que se vendió, 
mostrar en una columna si tienen convenio para los descuentos y de cuanto 
consta el descuento, así mismo en otra columna el vendedor*/

SELECT
sucursal.suc_nombre,sucursal.suc_direccion,
sucursal.activo_convenio,venta.venta_fecha,venta.venta_descuento,
farmaco.farmaco_nombre,farmaco.farmaco_precio,
detalleventa.detav_cantidad,cliente.cli_cedula,cliente.cli_nombres,
cliente.cli_prescripcion_,vendedor.ven_nombres
FROM sucursal inner join farmaco on sucursal.suc_id= farmaco.suc_id
inner join detalleventa on detalleventa.farmaco_id = farmaco.farmaco_id
inner join vendedor on vendedor.ven_id = detalleventa.ven_id
inner join venta on venta.venta_id = detalleventa.venta_id
inner join cliente on cliente.cli_id = detalleventa.cli_id









