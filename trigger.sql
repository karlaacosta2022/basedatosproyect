CREATE OR REPLACE FUNCTION trigger_cantidadfarmaco() RETURNS TRIGGER 
AS
$trigger_cantidadfarmaco$
 DECLARE
 cantidad INT;	
 BEGIN
	select 
	detalleventa.detav_cantidad into cantidad
	FROM sucursal inner join farmaco on sucursal.suc_id= farmaco.suc_id
	inner join detalleventa on detalleventa.farmaco_id = farmaco.farmaco_id
	inner join vendedor on vendedor.ven_id = detalleventa.ven_id
	inner join venta on venta.venta_id = detalleventa.venta_id
	inner join cliente on cliente.cli_id = detalleventa.cli_id
	WHERE detalleventa.detav_cantidad= new.detav_cantidad;
	if(cantidad > 50) THEN
	RAISE EXCEPTION 'Excede el pedido máximo de fármaco';
	END if;
	RETURN NEW;
END;
$trigger_cantidadfarmaco$
LANGUAGE plpgsql;
create trigger trigger_cantidadfarmaco before insert 
on detalleventa for EACH ROW
execute procedure trigger_cantidadfarmaco();
	
INSERT INTO detalleventa VALUES (106,1,2,1,1,60);


	