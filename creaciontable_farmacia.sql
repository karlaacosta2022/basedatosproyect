
/*==============================================================*/
/* Table: ADQUISICIONFARMACO                                    */
/*==============================================================*/
create table ADQUISICIONFARMACO (
ADQUI_ID             INT4                 not null,
FARMACO_ID           INT4                 not null,
PROV_ID              INT4                 not null,
ADQUI_FECHA          DATE                 not null,
ADQUI_COSTO          DECIMAL(6,2)         not null,
constraint PK_ADQUISICIONFARMACO primary key (ADQUI_ID)
);

/*==============================================================*/
/* Index: ADQUISICIONFARMACO_PK                                 */
/*==============================================================*/
create unique index ADQUISICIONFARMACO_PK on ADQUISICIONFARMACO (
ADQUI_ID
);

/*==============================================================*/
/* Index: FARMACO_ADQUISICION_FK                                */
/*==============================================================*/
create  index FARMACO_ADQUISICION_FK on ADQUISICIONFARMACO (
FARMACO_ID
);

/*==============================================================*/
/* Index: PROVEEDOR_ADQUISICION_FK                              */
/*==============================================================*/
create  index PROVEEDOR_ADQUISICION_FK on ADQUISICIONFARMACO (
PROV_ID
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
CLI_ID               INT4                 not null,
CLI_CEDULA           VARCHAR(10)          not null,
CLI_NOMBRES          VARCHAR(40)          not null,
CLI_APELLIDOS        VARCHAR(40)          not null,
CLI_DIRECCION        VARCHAR(40)          not null,
CLI_TELEFONO         VARCHAR(10)          not null,
CLI_EMAIL            VARCHAR(40)          null,
CLI_FECHANACIMIENTO  DATE                 not null,
CLI_PRESCRIPCION_    BOOL                 not null,
constraint PK_CLIENTE primary key (CLI_ID)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
CLI_ID
);

/*==============================================================*/
/* Table: DETALLEVENTA                                          */
/*==============================================================*/
create table DETALLEVENTA (
DETAV_ID             INT4                 not null,
VENTA_ID             INT4                 not null,
CLI_ID               INT4                 not null,
FARMACO_ID           INT4                 not null,
VEN_ID               INT4                 not null,
DETAV_CANTIDAD       INT4                 not null,
constraint PK_DETALLEVENTA primary key (DETAV_ID)
);

/*==============================================================*/
/* Index: DETALLEVENTA_PK                                       */
/*==============================================================*/
create unique index DETALLEVENTA_PK on DETALLEVENTA (
DETAV_ID
);

/*==============================================================*/
/* Index: VENTA_DETAVENTA_FK                                    */
/*==============================================================*/
create  index VENTA_DETAVENTA_FK on DETALLEVENTA (
VENTA_ID
);

/*==============================================================*/
/* Index: CLIENTE_DETAVENTA_FK                                  */
/*==============================================================*/
create  index CLIENTE_DETAVENTA_FK on DETALLEVENTA (
CLI_ID
);

/*==============================================================*/
/* Index: FARMACO_DETAVENTA_FK                                  */
/*==============================================================*/
create  index FARMACO_DETAVENTA_FK on DETALLEVENTA (
FARMACO_ID
);

/*==============================================================*/
/* Index: VENDEDOR_DETAVENTA_FK                                 */
/*==============================================================*/
create  index VENDEDOR_DETAVENTA_FK on DETALLEVENTA (
VEN_ID
);

/*==============================================================*/
/* Table: ESTADOVENDEDOR                                        */
/*==============================================================*/
create table ESTADOVENDEDOR (
ESTADO_ID            INT4                 not null,
ESTADO_DESCRIPCION   VARCHAR(40)          not null,
constraint PK_ESTADOVENDEDOR primary key (ESTADO_ID)
);

/*==============================================================*/
/* Index: ESTADOVENDEDOR_PK                                     */
/*==============================================================*/
create unique index ESTADOVENDEDOR_PK on ESTADOVENDEDOR (
ESTADO_ID
);

/*==============================================================*/
/* Table: FARMACEUTICA                                          */
/*==============================================================*/
create table FARMACEUTICA (
FARMA_ID             INT4                 not null,
FARMA_NOMBRE         VARCHAR(15)          not null,
FARMA_DIRECCION      VARCHAR(40)          not null,
FARMA_TELEFONO       VARCHAR(10)          not null,
constraint PK_FARMACEUTICA primary key (FARMA_ID)
);

/*==============================================================*/
/* Index: FARMACEUTICA_PK                                       */
/*==============================================================*/
create unique index FARMACEUTICA_PK on FARMACEUTICA (
FARMA_ID
);

/*==============================================================*/
/* Table: FARMACO                                               */
/*==============================================================*/
create table FARMACO (
FARMACO_ID           INT4                 not null,
SUC_ID               INT4                 not null,
FARMACO_NOMBRE       VARCHAR(40)          not null,
FARMACO_PRECIO       DECIMAL(6,2)         not null,
FARMACO_FECHAVENCIMEINTO DATE                 not null,
FARMACO_STOCK        INT4                 not null,
constraint PK_FARMACO primary key (FARMACO_ID)
);

/*==============================================================*/
/* Index: FARMACO_PK                                            */
/*==============================================================*/
create unique index FARMACO_PK on FARMACO (
FARMACO_ID
);

/*==============================================================*/
/* Index: SUC_PROD_FK                                           */
/*==============================================================*/
create  index SUC_PROD_FK on FARMACO (
SUC_ID
);

/*==============================================================*/
/* Table: HORARIOLABORAL                                        */
/*==============================================================*/
create table HORARIOLABORAL (
HORARIO_ID           INT4                 not null,
HORARIO_ENTRADA      TIME                 not null,
HORARIO_SALIDA       TIME                 not null,
PAGO_HORA            DECIMAL(6,2)         not null,
PAGO_HORAEXTRA       DECIMAL(6,2)         null,
PAGO_TOTAL           DECIMAL(7,2)         null,
constraint PK_HORARIOLABORAL primary key (HORARIO_ID)
);

/*==============================================================*/
/* Index: HORARIOLABORAL_PK                                     */
/*==============================================================*/
create unique index HORARIOLABORAL_PK on HORARIOLABORAL (
HORARIO_ID
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR (
PROV_ID              INT4                 not null,
FARMA_ID             INT4                 not null,
PROV_NOMBRE          VARCHAR(40)          not null,
PROV_DIRECCION       VARCHAR(40)          not null,
PROV_TELEFONO        VARCHAR(10)          not null,
PROV_EMAIL           VARCHAR(40)          not null,
constraint PK_PROVEEDOR primary key (PROV_ID)
);

/*==============================================================*/
/* Index: PROVEEDOR_PK                                          */
/*==============================================================*/
create unique index PROVEEDOR_PK on PROVEEDOR (
PROV_ID
);

/*==============================================================*/
/* Index: FARMECEUTICA_PROVEEDOR_FK                             */
/*==============================================================*/
create  index FARMECEUTICA_PROVEEDOR_FK on PROVEEDOR (
FARMA_ID
);

/*==============================================================*/
/* Table: SUCURSAL                                              */
/*==============================================================*/
create table SUCURSAL (
SUC_ID               INT4                 not null,
SUC_NOMBRE           VARCHAR(40)          not null,
SUC_DIRECCION        VARCHAR(40)          not null,
SUC_TELEFONO         VARCHAR(10)          not null,
ACTIVO_CONVENIO      BOOL                 not null,
constraint PK_SUCURSAL primary key (SUC_ID)
);

/*==============================================================*/
/* Index: SUCURSAL_PK                                           */
/*==============================================================*/
create unique index SUCURSAL_PK on SUCURSAL (
SUC_ID
);

/*==============================================================*/
/* Table: VENDEDOR                                              */
/*==============================================================*/
create table VENDEDOR (
VEN_ID               INT4                 not null,
SUC_ID               INT4                 not null,
ESTADO_ID            INT4                 not null,
HORARIO_ID           INT4                 not null,
VEN_NOMBRES          VARCHAR(40)          not null,
VEN_APELLIDOS        VARCHAR(40)          not null,
VEN_FECHANACIMIENTO  DATE                 not null,
VEN_FECHAINGRESO     DATE                 not null,
VEN_SUPERVISOR       INT4                 null,
constraint PK_VENDEDOR primary key (VEN_ID)
);

/*==============================================================*/
/* Index: VENDEDOR_PK                                           */
/*==============================================================*/
create unique index VENDEDOR_PK on VENDEDOR (
VEN_ID
);

/*==============================================================*/
/* Index: SUCURSAL_VENTA_FK                                     */
/*==============================================================*/
create  index SUCURSAL_VENTA_FK on VENDEDOR (
SUC_ID
);

/*==============================================================*/
/* Index: ESTADOV_VENDEDOR_FK                                   */
/*==============================================================*/
create  index ESTADOV_VENDEDOR_FK on VENDEDOR (
ESTADO_ID
);

/*==============================================================*/
/* Index: HORARIO_VENDEDOR_FK                                   */
/*==============================================================*/
create  index HORARIO_VENDEDOR_FK on VENDEDOR (
HORARIO_ID
);

/*==============================================================*/
/* Table: VENTA                                                 */
/*==============================================================*/
create table VENTA (
VENTA_ID             INT4                 not null,
VENTA_FECHA          DATE                 not null,
VENTA_DESCUENTO      DECIMAL(6,2)         null,
constraint PK_VENTA primary key (VENTA_ID)
);

/*==============================================================*/
/* Index: VENTA_PK                                              */
/*==============================================================*/
create unique index VENTA_PK on VENTA (
VENTA_ID
);

alter table ADQUISICIONFARMACO
   add constraint FK_ADQUISIC_FARMACO_A_FARMACO foreign key (FARMACO_ID)
      references FARMACO (FARMACO_ID)
      on delete restrict on update restrict;

alter table ADQUISICIONFARMACO
   add constraint FK_ADQUISIC_PROVEEDOR_PROVEEDO foreign key (PROV_ID)
      references PROVEEDOR (PROV_ID)
      on delete restrict on update restrict;

alter table DETALLEVENTA
   add constraint FK_DETALLEV_CLIENTE_D_CLIENTE foreign key (CLI_ID)
      references CLIENTE (CLI_ID)
      on delete restrict on update restrict;

alter table DETALLEVENTA
   add constraint FK_DETALLEV_FARMACO_D_FARMACO foreign key (FARMACO_ID)
      references FARMACO (FARMACO_ID)
      on delete restrict on update restrict;

alter table DETALLEVENTA
   add constraint FK_DETALLEV_VENDEDOR__VENDEDOR foreign key (VEN_ID)
      references VENDEDOR (VEN_ID)
      on delete restrict on update restrict;

alter table DETALLEVENTA
   add constraint FK_DETALLEV_VENTA_DET_VENTA foreign key (VENTA_ID)
      references VENTA (VENTA_ID)
      on delete restrict on update restrict;

alter table FARMACO
   add constraint FK_FARMACO_SUC_PROD_SUCURSAL foreign key (SUC_ID)
      references SUCURSAL (SUC_ID)
      on delete restrict on update restrict;

alter table PROVEEDOR
   add constraint FK_PROVEEDO_FARMECEUT_FARMACEU foreign key (FARMA_ID)
      references FARMACEUTICA (FARMA_ID)
      on delete restrict on update restrict;

alter table VENDEDOR
   add constraint FK_VENDEDOR_ESTADOV_V_ESTADOVE foreign key (ESTADO_ID)
      references ESTADOVENDEDOR (ESTADO_ID)
      on delete restrict on update restrict;

alter table VENDEDOR
   add constraint FK_VENDEDOR_HORARIO_V_HORARIOL foreign key (HORARIO_ID)
      references HORARIOLABORAL (HORARIO_ID)
      on delete restrict on update restrict;

alter table VENDEDOR
   add constraint FK_VENDEDOR_SUCURSAL__SUCURSAL foreign key (SUC_ID)
      references SUCURSAL (SUC_ID)
      on delete restrict on update restrict;

